## _Deprecation Notice_

This Pre-Built Automation has been deprecated as of 12-13-2023 and will be end of life on 12-13-2024. The capabilities of this Pre-Built Automation have been replaced by the [Itential AWS - EC2 - REST Project](https://gitlab.com/itentialopensource/pre-built-automations/aws-ec2-rest).

# AWS EC2 Instance Creation

## Table of Contents

- [AWS EC2 Instance Creation](#aws-ec2-instance-creation)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Requirements](#requirements)
  - [Features](#features)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
    - [Input Variables](#input-variables)
  - [Additional Information](#additional-information)

## Overview

This Pre-Built integrates with the [AWS EC2 Open Source Adapter](https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2) to create an Elastic Compute Cloud (EC2) instance within an Amazon Web Services (AWS) Virtual Private Cloud (VPC).

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2023.1`
* [AWS EC2 Open Source Adapter](https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2)
  * `^0.6.3`

## Requirements

This Pre-Built requires the following:

* A [AWS EC2 Open Source Adapter](https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2)
* A AWS account that has adequate permission to create an EC2 instance

## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows user to create an EC2 instance by leveraging a simple form to define: 
  * Region 
  * Amazon Machine Image (AMI)
  * Instance Type (size)
  * VPC ID
  * Security Group ID
  * Subnet ID
  * Key Name

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).


## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Automation `AWS EC2 Instance Creation` or call `AWS EC2 Instance Creation` in a childJob task or as a stand alone workflow.

### Input Variables
_Example_

```json
{
  "region": "Specific Region in which the EC2 instance will be located (for example us-east-1, eu-west-1, etc.)",
  "ami": "Amazon Machine Instance that will be leveraged to create the EC2 instance",
  "instanceType": "Type of EC2 instance to be created (for example T4g, T3, C6g, C5, etc.)",
  "vpcId": "ID associated with your VPC (for example vpc-1a2b3c4d)",
  "securityGroupId": "ID for the security group that you would like to associate with your EC2 instance (for example sg-xxxxxxxx)",
  "subnetId": "ID of the subnet to launch the instance into"
  "keyName": "Name of the key pair"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
