
## 0.0.18 [03-18-2024]

* Update metadata.json

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!23

---

## 0.0.17 [02-05-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!19

---

## 0.0.16 [01-03-2024]

* Removes relative links

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!15

---

## 0.0.15 [01-03-2024]

* Remove img tag from markdown file in master

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!11

---

## 0.0.14 [12-13-2023]

* Add deprecation notice

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!9

---

## 0.0.13 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!8

---

## 0.0.12 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!7

---

## 0.0.11 [01-06-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!6

---

## 0.0.10 [07-09-2021]

* Update README.md, package.json files

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!4

---

## 0.0.9 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/aws-ec2-instance-creation!1

---

## 0.0.8 [04-15-2021]

* Update README.md, images/aws_ec2_instance_creation_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/aws-ec2-instance-creation!3

---

## 0.0.7 [03-23-2021]

* Update README.md, images/aws_ec2_instance_creation_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/aws-ec2-instance-creation!3

---

## 0.0.6 [03-10-2021]

* patch/2021-03-10T06-56-44

See merge request itential/sales-engineer/selabprebuilts/aws-ec2-instance-creation!2

---

## 0.0.5 [03-10-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/create-ec2instanceaws!1

---

## 0.0.4 [02-24-2021]

* Bug fixes and performance improvements

See commit f1b1d4f

---

## 0.0.3 [02-24-2021]

* Bug fixes and performance improvements

See commit 1fe537d

---

## 0.0.2 [02-24-2021]

* Bug fixes and performance improvements

See commit e0ebc26

---\n\n\n\n\n\n\n
